import angular from 'angular';

class ConfigService {

  constructor($rootScope) {
    'ngInject';
    this.apiUrl = 'https://api.themoviedb.org/3';
    this.apiKey = 'ae6f0328593d4bd23691db639f46a20a';
    this.pageLimit = 10;
    this.$rootScope = $rootScope;
  }

  setToken(token) {
    console.log('Token has been set');
    this.token = token;
    // Call the event on setting the user token
    this.$rootScope.$broadcast('tokenReady');
  }

  // The function is being executed when token is ready
  // It could used when you want start some request after this only
  onTokenReady(cb) {
    this.$rootScope.$on('tokenReady', () => {
      console.log('token is ready');
      cb();
    });
  }

}

ConfigService.$inject = ['$rootScope'];

export default angular.module('services.config-service', [])
  .service('ConfigService', ConfigService)
  .name;
