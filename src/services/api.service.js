import _ from '../../node_modules/underscore/underscore-min';
import angular from 'angular';

class ApiService {

  constructor(ConfigService, $rootScope, $http, $q) {
    'ngInject';
    this.config = ConfigService;
    this.http = $http;
    this.promise = $q;
    this.$rootScope = $rootScope;

    if (!this.config.token) {
      this.http.get(this.config.apiUrl + '/authentication/token/new?api_key=' + this.config.apiKey)
        .then((res) => {
          if (_.isObject(res) && _.isObject(res.data) && res.data.request_token) {
            this.config.setToken(res.data.request_token);
          } else {
            throw 'Wierd reply from the server';
          }
        })
        .catch((err) => console.warn('Error on getting the token ' + err));
    }

    if (!_.isObject(this.genres) || _.isEmpty(this.genres)) {
      this.genresProc = true;
      this.requestFroGenres().then((res) => {
        // In underscore JS library _.isObject(array) and _.isObject(object) will give the same reult
        // so I use native array property to check the given obj type is array or not
        // otherwise the last check of array length will crash with error
        if (_.isObject(res) && _.isObject(res.data) && Array.isArray(res.data.genres) && res.data.genres.length > 0) {
          this.genres = res.data.genres;
          this.$rootScope.$broadcast('genresReady');
        } else {
          this.genres = [];
        }
      }).catch((err) => console.warn('Wierd reply from the server on genres getting ' + err));
    }
  }

  getAvailableGenres() {
    return this.promise((resolve) => {
      if (Array.isArray(this.genres) && this.genres.length > 0) {
        resolve(this.genres);
      } else if (this.genresProc) {
        // Here we see the process of genres getting was started already
        // and we subscribe for the result
        this.$rootScope.$on('genresReady', () => resolve(this.genres));
      } else {
        this.requestFroGenres((res) => {
          if (_.isObject(res) && _.isObject(res.data) && Array.isArray(res.data.genres) && res.data.genres.length > 0) {
            this.genres = res.data.genres;
          } else {
            this.genres = [];
          }
          resolve(this.genres);
        }).then().catch(() => resolve([]));
      }
    });
  }

  requestFroGenres() {
    return this.promise((resolve, reject) => {
      let _url = this.config.apiUrl + '/genre/movie/list?api_key=' + this.config.apiKey;
      if (!this.config.token) {
        this.config.onTokenReady(() => {
          this.http.get(_url).then(resolve).catch(reject);
        });
      } else {
        this.http.get(_url).then(resolve).catch(reject);
      }
    });
  }

  getMoviesList(pageNum, genreID) {
    pageNum = !_.isNumber(pageNum) || pageNum <= 0 ? 1 : pageNum;
    genreID = !_.isNumber(genreID) || genreID <= 0 ? '' : genreID;
    return this.promise((resolve, reject) => {
      let _url = this.config.apiUrl + '/discover/movie?api_key=' + this.config.apiKey + '&sort_by=popularity.desc&page=' + pageNum;
      if (genreID) {
        _url += '&with_genres=' + genreID;
      }
      // Here I added getting results as callback function if token is not defined
      // In this particular case this is not necessary cause a bit later I found out I could get the movie list without a token
      // Token is needed for user login validation, ask for user permissions and stuff like this
      // Bit in general it could be getting some important info instead the token getting
      // And in that case is very useful to be able start some requests after all this stuff is ready
      if (!this.config.token) {
        this.config.onTokenReady(() => {
          console.log(this.config.token, 'show me the token');
          this.http.get(_url).then(resolve).catch(reject);
        });
      } else {
        this.http.get(_url).then(resolve).catch(reject);
      }
    });
  }

  getMoviesByID(id) {
    let _url = this.config.apiUrl + '/movie/' + id + '?api_key=' + this.config.apiKey;
    return this.promise((resolve, reject) => {
      if (!this.config.token) {
        this.config.onTokenReady(() => {
          console.log(this.config.token, 'show me the token');
          this.http.get(_url).then(resolve).catch(reject);
        });
      } else {
        this.http.get(_url).then(resolve).catch(reject);
      }
    });
  }

  getMoviesBySearch(query) {
    let _url = this.config.apiUrl + '/search/movie?api_key=' + this.config.apiKey + '&query=' + query;
    return this.promise((resolve, reject) => {
      if (!this.config.token) {
        this.config.onTokenReady(() => {
          console.log(this.config.token, 'show me the token');
          this.http.get(_url).then(resolve).catch(reject);
        });
      } else {
        this.http.get(_url).then(resolve).catch(reject);
      }
    });
  }

}

ApiService.$inject = ['ConfigService', '$rootScope', '$http', '$q'];

export default angular.module('services.api.service', [])
  .service('ApiService', ApiService)
  .name;
