import _ from '../../../../node_modules/underscore/underscore-min';
export default class MovListController {

  constructor(ApiService, $scope) {
    this.error = '';
    this.serv = ApiService;

    // Watching for the search query input
    $scope.$watch(() => this.searchQuery, (newVal, oldVal) => {
      if (newVal && newVal !== oldVal) {
        this.getMoviesBySearch(newVal);
      } else if (!newVal && oldVal) {
        this.getMoviesDefault();
      }
    });

    this.getMoviesDefault();

    this.serv.getAvailableGenres().then(res => {
      console.log('genres got');
      this.genres = res;
    });

  }

  getMoviesByGenre(id) {
    this.serv.getMoviesList('', id).then((res) => {
      if (_.isObject(res) && _.isObject(res.data) && Array.isArray(res.data.results)) {
        this.movies = res.data.results;
      } else {
        this.error = 'Wrong server answer';
      }
    }).catch((err) => this.error = err);
  }

  getMoviesBySearch(query) {
    this.serv.getMoviesBySearch(query).then((res) => {
      if (_.isObject(res) && _.isObject(res.data) && Array.isArray(res.data.results)) {
        this.movies = res.data.results;
      } else {
        this.error = 'Wrong server answer';
      }
    }).catch((err) => this.error = err);
  }

  getMoviesDefault() {
    this.serv.getMoviesList().then((res) => {
      if (_.isObject(res) && _.isObject(res.data) && Array.isArray(res.data.results)) {
        this.movies = res.data.results;
      } else {
        this.error = 'Wrong server answer';
      }
    }).catch((err) => this.error = err);
  }

}

MovListController.$inject = ['ApiService', '$scope'];
