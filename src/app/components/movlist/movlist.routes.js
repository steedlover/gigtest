'use strict';

routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
  $stateProvider
    .state('movlist', {
      url: '/',
      template: require('./movlist.html'),
      controller: 'MovListController',
      controllerAs: 'ml'
    });
}
