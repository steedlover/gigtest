'use strict';

import './movlist.styl';
import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './movlist.routes';
import MovListController from './movlist.controller';

export default angular.module('app.movlist', [uirouter])
  .config(routing)
  .controller('MovListController', MovListController)
  .name;
