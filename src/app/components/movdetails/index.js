'use strict';

import './movdetails.styl';
import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './movdetails.routes';
import MovDetailsController from './movdetails.controller';

export default angular.module('app.movdetails', [uirouter])
  .config(routing)
  .controller('MovDetailsController', MovDetailsController)
  .name;
