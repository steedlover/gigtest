import _ from '../../../../node_modules/underscore/underscore-min';
export default class MovDetailsController {

  constructor(ApiService, $stateParams) {
    'ngInject';
    this.movie = {};
    this.error = '';
    this.loading = true;
    if (_.isObject($stateParams) && $stateParams.id) {
      ApiService.getMoviesByID($stateParams.id).then((res) => {
        if (_.isObject(res) && res.status === 200 && _.isObject(res.data)) {
          this.movie = res.data;
          this.loading = false;
        }
      });
    } else {
      this.error = 'Something went wrong';
      this.loading = false;
    }
  }

}

MovDetailsController.$inject = ['ApiService', '$stateParams'];
