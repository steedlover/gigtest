'use strict';

routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
  $stateProvider
    .state('movdetails', {
      url: '/movie/:id',
      template: require('./movdetails.html'),
      controller: 'MovDetailsController',
      controllerAs: 'md'
    });
}
