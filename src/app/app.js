'use strict';

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from '../app.config';

import 'bootstrap/dist/css/bootstrap.css';
import '../style/styles.styl';

import ConfigService from '../services/config.service';
import ApiService from '../services/api.service';

import movlist from './components/movlist';
import movdetails from './components/movdetails';

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [uirouter, ApiService, ConfigService, movlist, movdetails])
  .config(routing);

export default MODULE_NAME;
