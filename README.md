### Quick start

git clone https://steedlover@bitbucket.org/steedlover/gigtest.git
npm i
npm start

Application will be started on localhost:8080

### Styles

I connected 'stylus' css precompiler.
Separated variables file from other. I wanted to create rule within webpack.config for loading content of the 'style' folder automatically.
But it's required even longer time and I decided to import this file where it's necessary. So each my styl file within a component has such an import.

### Movies list page

In ideal case this page has paginator and when we are getting back to this page last query, page number(if defined) is being restored.

### Filtering by genre

I don't know why but it's working strange. For example you select some genre but the movies in the doesn't have this genre in their lists.
In this case I just display the movies list what I get from the API. Maybe more accurate preparing a query string for the server could be solve this issue.
I could not pay a lot of attention to the problem because of short time.

### Services

I wanted to realize this structure of getting the data:
1) get auth token (it's not necessary in this case. Data could be retrieved without token)
2) get calloborate data such as genres list. This object will be needed on the movies list getting and displaying
3) get main data which depends on the page. Movies list for main page, detailed movie object in the dscsrion page
For this purpose was created callback function which allows to set a proper order of all data getting.

Furhermore in ideal case all the data should be stored locally. It will allow to avoid double requests to the API.

### Summary
I had a lot of fun while I was working on your task :-) I look forward to getting feedback from you